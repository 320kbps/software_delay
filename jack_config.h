#pragma once
#include <jack/jack.h> 

void set_up_jack_client(jack_client_t** client, void (shutdown_callback(void *)), int (process_callback(unsigned int, void *)));

void set_up_client_ports(jack_client_t **client, jack_port_t **input_port_l, jack_port_t **input_port_r, jack_port_t **output_port_l, jack_port_t **output_port_r);

void connect_effect_output(jack_client_t **client, jack_port_t *output_port_l, jack_port_t *output_port_r);

