#pragma once
#include "convenience.h"

typedef struct {
	double fb_gain; 	//feedback gain
	int N;				//delay
	int dline_length;	//delay line length
	int widx;			//write index
	sample_t* dline;	//delay_line
} comb_data;

void comb_filter(comb_data *cfd, int nframes, sample_t *in, sample_t *out);
