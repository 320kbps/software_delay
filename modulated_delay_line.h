#pragma once
#include "convenience.h"

typedef struct{
   sample_t *dline_l;
   sample_t *dline_r;
   int dline_length;
   int widx;
   double pnoise;
   double ff_gain;
   double fb_gain;
   double bypass_gain;
   double t;
   double T;
   double modfreq;
   double modamp;
   double modoffset;
} mod_delay_data;

double lin_interp(double y0, double y1, double dt);

void mod_delay_process(
        int nframes,
        sample_t *in_l,
        sample_t *in_r,
        sample_t *out_l,
        sample_t *out_r
        );
