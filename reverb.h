#pragma once
#include "convenience.h"

void reverb_process(jack_nframes_t nframes, sample_t *in_l, sample_t *in_r,
                    sample_t *out_l, sample_t *out_r);
