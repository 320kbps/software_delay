#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include "comb.h"

void comb_filter(comb_data *cfd, int nframes, sample_t *in, sample_t *out)
{
	int ridx = cfd->widx - cfd->N;
	if(ridx < 0) ridx = ridx + cfd->dline_length; 
	
	for(int n = 0; n < nframes; n++){
		out[n] = in[n]- cfd->dline[ridx]*cfd->fb_gain;
		cfd->dline[cfd->widx] = out[n];
		cfd->widx = (cfd->widx + 1) % cfd->dline_length;
		ridx = (ridx + 1) % cfd->dline_length;			
	}	
}

