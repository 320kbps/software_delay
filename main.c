#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <jack/jack.h>
#include "jack_config.h"

#include "modulated_delay_line.h"
#include "reverb.h"

#include "gui.h"
#include "defaults.h"

int mode = 0; //0: #nofilter, 1: delay line, 2: reverberation
int pmode;

typedef jack_default_audio_sample_t sample_t;
jack_port_t *input_port_l, *input_port_r, *output_port_l, *output_port_r;
jack_client_t *client;

sample_t low_pass_buffer_l[400];
sample_t low_pass_buffer_r[400];

//This function is called once per audio cycle
int process(jack_nframes_t nframes, void *arg){
    jack_default_audio_sample_t *in_l, *in_r, *out_l, *out_r;

    //get pointers to the input and output buffers
    in_l = jack_port_get_buffer(input_port_l, nframes);
    in_r = jack_port_get_buffer(input_port_r, nframes);
    out_l = jack_port_get_buffer(output_port_l, nframes);
    out_r = jack_port_get_buffer(output_port_r, nframes);

	switch(mode){
		case 0:
			memcpy(out_l, in_l, nframes * sizeof(sample_t));
			memcpy(out_r, in_r, nframes * sizeof(sample_t));
			break;
		case 1: //delay line
			mod_delay_process(nframes, in_l, in_r, out_l, out_r);
			break;
		case 2: //reverb
			reverb_process(nframes, in_l, in_r, out_l, out_r);
			break;
		}
	return(0);
}

void jack_shutdown(void *arg)
{
    exit(1);
}

int main(int argc, char* argv[])
{
	pmode = mode;
	gtk_init (&argc,&argv);
	init_gui();

    //connect to jack
    set_up_jack_client(&client, jack_shutdown, process);
    set_up_client_ports(
            &client,
            &input_port_l,
            &input_port_r,
            &output_port_l,
            &output_port_r);

    //tell the jack server we're ready to process data
    if(jack_activate(client))
    {
        fprintf(stderr, "can't activate the client\n");
        exit(1);
    }

    // hook up the output of the effect to the default sink
    connect_effect_output(&client, output_port_l, output_port_r);
    //input is left to be connected manually for now

	gtk_main();

    //we'll never get here, but if there was another way to kill this program,
    //these need to be called there
    jack_client_close(client);
    exit(0);
}
