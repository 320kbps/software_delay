#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include "allpass.h"
#include "comb.h"
#include "modulated_delay_line.h"
#include "defaults.h"

extern int mode;
extern double dry_proportion;

extern allpass_data left_ap1_data, right_ap1_data;
extern allpass_data left_ap2_data, right_ap2_data;
extern allpass_data left_ap3_data, right_ap3_data;
extern comb_data left_comb1_data, right_comb1_data;
extern comb_data left_comb2_data, right_comb2_data;
extern comb_data left_comb3_data, right_comb3_data;
extern comb_data left_comb4_data, right_comb4_data;

extern mod_delay_data mdd;

void set_values_callback(GtkObject *adj, gpointer data);

void destroy(GtkWidget *widget, gpointer data);

void make_scale(GtkWidget *table, double min, double max, double step, const char *name, int row, int col, double *parameter);

void make_multiparam_scale(GtkWidget *table, double min, double max, double def_val, double step, const char *name, int row, int col, double **parameter,  int num_parameters);

void init_gui();

void reverb_callback(GtkWidget *button, gpointer data);

void delay_line_callback(GtkWidget *button, gpointer data);

void gui_reverb(GtkWidget *table);

void gui_delay_line(GtkWidget *table);
