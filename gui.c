#include "gui.h"

void set_values_callback(GtkObject *adj, gpointer data)
{
	double value = (double) gtk_adjustment_get_value(GTK_ADJUSTMENT(adj));
	*((double *)data) = value;
}

void radio_callback(GtkWidget *button, gpointer data)
{
	mode = (int) data;
	g_print("Mode %i", (int) data);
}

void destroy(GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
}

void make_scale(GtkWidget *table, double min, double max, double step, const char *name, int pos,int col, double *parameter)
{
	GtkWidget *scale;
	GtkWidget *label;
	GtkObject *adj;

	adj = gtk_adjustment_new(*parameter, min, max, step, step, 1);

	scale = gtk_hscale_new(GTK_ADJUSTMENT(adj));
	if(step < 1){
		gtk_scale_set_digits(GTK_SCALE(scale),2);
	} else {
		gtk_scale_set_digits(GTK_SCALE(scale),0);
	}

	label = gtk_label_new(name);
	gtk_table_attach_defaults(GTK_TABLE(table),scale,col,col+2,pos,pos+1);
	gtk_table_attach_defaults(GTK_TABLE(table),label,col+2,col+3,pos,pos+1);
    g_signal_connect(adj, "value_changed", G_CALLBACK(set_values_callback), parameter);
}

void make_multiparam_scale(GtkWidget *table, double min, double max, double def_val, double step, const char *name, int row, int col, double **parameter,  int num_parameters)
{
	GtkWidget *scale;
	GtkWidget *label;
	GtkObject *adj;

	adj = gtk_adjustment_new(def_val, min, max, step, step, 1);

	scale = gtk_hscale_new(GTK_ADJUSTMENT(adj));
	gtk_scale_set_digits(GTK_SCALE(scale),2);
	label = gtk_label_new(name);
	gtk_table_attach_defaults(GTK_TABLE(table),scale,col,col+2,row,row+1);
	gtk_table_attach_defaults(GTK_TABLE(table),label,col+2,col+3,row,row+1);
    for (int i = 0; i < num_parameters; i++){
        printf("adding callback %i\n", i);
        g_signal_connect(adj, "value_changed", G_CALLBACK(set_values_callback), parameter[i]);
    }
}

void make_selector(GtkWidget *table){
	GtkWidget *radio0;
	GtkWidget *radio1;
	GtkWidget *radio2;

	radio0 = gtk_radio_button_new_with_label(NULL, "No Filter");
	radio1 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio0), "Delay Line");
	radio2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio0), "Reverberator");

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio0), TRUE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio1), FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio2), FALSE);

	gtk_table_attach_defaults(GTK_TABLE(table), radio0, 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), radio1, 1, 3, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), radio2, 4, 6, 0, 1);


	g_signal_connect(GTK_TOGGLE_BUTTON(radio0), "toggled", G_CALLBACK(radio_callback), (int*)0);
	g_signal_connect(GTK_TOGGLE_BUTTON(radio1), "toggled", G_CALLBACK(radio_callback), (int*)1);
	g_signal_connect(GTK_TOGGLE_BUTTON(radio2), "toggled", G_CALLBACK(radio_callback), (int*)2);
}

void gui_delay_line(GtkWidget *table){
	make_scale(table, 0, 2, 0.01, "Feedback Gain", 1, 1, &mdd.fb_gain);
	make_scale(table, 0, 2, 0.01, "Feedforward Gain", 2, 1, &mdd.ff_gain);
	make_scale(table, 0, 2, 0.01, "Bypass Gain", 3, 1, &mdd.bypass_gain);
	make_scale(table, 0, 1000, 10, "Delay", 4, 1, &mdd.modoffset);
	make_scale(table, 0, 10, 0.01, "Modulation Frequency", 5, 1, &mdd.modfreq);
	make_scale(table, 0, 200, 1, "Modulation Amplitude", 6, 1, &mdd.modamp);
}

void gui_reverb(GtkWidget *table){

	make_scale(table, 0, 2, 0.01, "Dry Proportion", 1, 4, &dry_proportion);

	double* ap1_gains[2] = {&(left_ap1_data.g), &(right_ap1_data.g)};
	make_multiparam_scale(table, 0, 2, AP1_DEFAULT_GAIN, 0.01, "AP1 Gain", 2, 4, ap1_gains, 2);

	double* ap2_gains[2] = {&(left_ap2_data.g), &(right_ap2_data.g)};
	make_multiparam_scale(table, 0, 2, AP2_DEFAULT_GAIN, 0.01, "AP2 Gain", 3, 4, ap2_gains, 2);

	double* ap3_gains[2] = {&(left_ap3_data.g), &(right_ap3_data.g)};
	make_multiparam_scale(table, 0, 2, AP3_DEFAULT_GAIN, 0.01, "AP3 Gain", 4, 4, ap3_gains, 2);

	double* comb1_gains[2] = {&(left_comb1_data.fb_gain), &(right_comb1_data.fb_gain)};
	make_multiparam_scale(table, 0, 2, COMB1_DEFAULT_GAIN, 0.01, "Comb 1 Gain", 5, 4, comb1_gains, 2);

	double* comb2_gains[2] = {&(left_comb2_data.fb_gain), &(right_comb2_data.fb_gain)};
	make_multiparam_scale(table, 0, 2, COMB2_DEFAULT_GAIN, 0.01, "Comb 2 Gain", 6, 4, comb2_gains, 2);

	double* comb3_gains[2] = {&(left_comb3_data.fb_gain), &(right_comb3_data.fb_gain)};
	make_multiparam_scale(table, 0, 2, COMB3_DEFAULT_GAIN, 0.01, "Comb 3 Gain", 7, 4, comb3_gains, 2);

	double* comb4_gains[2] = {&(left_comb4_data.fb_gain), &(right_comb4_data.fb_gain)};
	make_multiparam_scale(table, 0, 2, COMB4_DEFAULT_GAIN, 0.01, "Comb 4 Gain", 8, 4, comb4_gains, 2);

}

void init_gui()
{
	GtkWidget *window;
	GtkWidget *table;


	//window
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title( GTK_WINDOW(window), "ECE 484");
	gtk_container_set_border_width (GTK_CONTAINER(window),20);
	g_signal_connect(window, "destroy", G_CALLBACK (destroy), NULL);
	table = gtk_table_new(10,6, TRUE);

	make_selector(table);

	gui_delay_line(table);
	gui_reverb(table);

	gtk_container_add(GTK_CONTAINER(window), table);
	gtk_widget_show_all(window);
}
