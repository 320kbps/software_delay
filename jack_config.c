#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "jack_config.h"

void set_up_jack_client(jack_client_t** client, void (shutdown_callback(void *)), int (process_callback(unsigned int, void *)))
{
    const char *client_name = "Over and Over";
    const char *server_name = NULL;

    jack_options_t options = JackNullOption;
    jack_status_t status;

    *client = jack_client_open(client_name, options, &status, server_name);

    if(client == NULL) 
    {
        fprintf(stderr, "jack_client_open() failed. Status: 0x%x\n", status);
        exit(1);
    }

    if(status & JackServerStarted) 
    {
        fprintf(stderr, "Jack server started\n");
    }

    if(status & JackNameNotUnique) 
    {
        client_name = jack_get_client_name(*client);
        fprintf(stderr, "unique name %s was assigned\n", client_name);
    }

    //assign our process callback

    jack_set_process_callback(*client, process_callback, 0);

    //set up the shutdown callback

    jack_on_shutdown(*client, shutdown_callback, 0);

    printf("Client configured. Engine sample rate: %d\n", jack_get_sample_rate(*client));
}

void set_up_client_ports(jack_client_t **client, jack_port_t **input_port_l, jack_port_t **input_port_r, jack_port_t **output_port_l, jack_port_t **output_port_r) {

    // set up the ports
    *input_port_l = jack_port_register(*client, "input_l", JACK_DEFAULT_AUDIO_TYPE,
            JackPortIsInput, 0);
    *input_port_r = jack_port_register(*client, "input_r", JACK_DEFAULT_AUDIO_TYPE,
            JackPortIsInput, 0);
    *output_port_l = jack_port_register(*client, "output_l", JACK_DEFAULT_AUDIO_TYPE,
            JackPortIsOutput, 0);
    *output_port_r = jack_port_register(*client, "output_r", JACK_DEFAULT_AUDIO_TYPE,
            JackPortIsOutput, 0);

    if( (*input_port_l == NULL) || (*input_port_r == NULL) || (*output_port_l == NULL) 
        || (*output_port_r == NULL))
    {
        fprintf(stderr, "jack ports not available\n");
        exit(1);
    }

}
    
    //connect the effect's input port - note that the flag direction refers to how
    //it attaches to the server
    /*
    ports = jack_get_ports(client, NULL, NULL, JackPortIsPhysical | JackPortIsOutput);

    if (ports == NULL)
    {
        fprintf(stderr, "no input port available\n");
        exit(1);
    }

    if( jack_connect(client, ports[0], jack_port_name(input_port_l)) ||
        jack_connect(client, ports[1], jack_port_name(input_port_r)))
    {
        fprintf(stderr, "cannot connect input ports\n");
        exit(1);
    }

    free(ports);
    */

void connect_effect_output(jack_client_t **client, jack_port_t *output_port_l, jack_port_t *output_port_r){
    
    const char **ports;

    //connect the input port
    ports = jack_get_ports(*client, NULL, NULL, JackPortIsPhysical | JackPortIsInput);

    if(ports == NULL)
    {
        fprintf(stderr, "no output port available\n");
        exit(1);
    }

    if( jack_connect(*client, jack_port_name(output_port_l), ports[0]) ||
        jack_connect(*client, jack_port_name(output_port_r), ports[1]))
    {
        fprintf(stderr, "cannot connect output ports\n");
        exit(1);
    }

    free(ports);

}
