// allpass filter implementation
#include <jack/jack.h>

#include "allpass.h"

void allpass_process(
        allpass_data *apd,
        int nframes,        //input block size
        sample_t *in,       //input data
        sample_t *out       //output data
        ){          

    int ridx = apd->widx - apd->N;
    if (ridx < 0) ridx = apd->dline_length + ridx;

    for(int i = 0; i < nframes; i++){
        out[i] = -apd->g * in[i] + apd->dline[ridx];
        apd->dline[apd->widx] = in[i] + apd->g * out[i];
        apd->widx = (apd->widx + 1) % apd->dline_length;
        ridx = (ridx + 1) % apd->dline_length;
    }
}
