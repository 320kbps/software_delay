#pragma once
#include "convenience.h"

typedef struct {
    double g;           //gain
    int N;              //delay
    int dline_length;   //delay line length
    int widx;           //write index
    sample_t* dline;    //delay line
} allpass_data;

void allpass_process(
        allpass_data *apd,
        int nframes,        //input block size
        sample_t *in,       //input data
        sample_t *out       //output data
        );       
