#pragma once

#define SAMPLING_RATE 44100
#define DLINE_LENGTH 2300

#define DL_FF_GAIN 1
#define DL_FB_GAIN 0
#define DL_BP_GAIN 0
#define	DL_MOD_FREQ 0.5 //Hz
#define DL_MOD_AMP 500 //samples
#define DL_MOD_OFF 1000 //samples

#define AP1_DEFAULT_GAIN 0.7
#define AP2_DEFAULT_GAIN 0.7
#define AP3_DEFAULT_GAIN 0.7

#define COMB1_DEFAULT_GAIN 0.773
#define COMB2_DEFAULT_GAIN 0.802
#define COMB3_DEFAULT_GAIN 0.735
#define COMB4_DEFAULT_GAIN 0.733
