#include <string.h>

#include "reverb.h"
#include "comb.h"
#include "allpass.h"
#include "defaults.h"


sample_t right_ap1_dline[500];
sample_t left_ap2_dline[500];
sample_t left_ap1_dline[500];
sample_t right_ap2_dline[500];
sample_t left_ap3_dline[500];
sample_t right_ap3_dline[500];

sample_t left_comb1_dline[2300];
sample_t right_comb1_dline[2300];
sample_t left_comb2_dline[2300];
sample_t right_comb2_dline[2300];
sample_t left_comb3_dline[2300];
sample_t right_comb3_dline[2300];
sample_t left_comb4_dline[2300];
sample_t right_comb4_dline[2300];

allpass_data left_ap1_data = {
    .g = AP1_DEFAULT_GAIN,
    .N = 347,
    .dline_length = 500,
    .widx = 0,
    .dline = left_ap1_dline
};

allpass_data right_ap1_data = {
    .g = AP1_DEFAULT_GAIN,
    .N = 374,
    .dline_length = 500,
    .widx = 0,
    .dline = right_ap1_dline
};

allpass_data left_ap2_data = {
    .g = AP2_DEFAULT_GAIN,
    .N = 113,
    .dline_length = 500,
    .widx = 0,
    .dline = left_ap2_dline
};

allpass_data right_ap2_data = {
    .g = AP2_DEFAULT_GAIN,
    .N = 113,
    .dline_length = 500,
    .widx = 0,
    .dline = right_ap2_dline
};

allpass_data left_ap3_data = {
    .g = AP3_DEFAULT_GAIN,
    .N = 37,
    .dline_length = 500,
    .widx = 0,
    .dline = left_ap3_dline
};

allpass_data right_ap3_data = {
    .g = AP3_DEFAULT_GAIN,
    .N = 37,
    .dline_length = 500,
    .widx = 0,
    .dline = right_ap3_dline
};

comb_data left_comb1_data = {
    .fb_gain = COMB1_DEFAULT_GAIN,
    .N = 1687,
    .dline_length = 2300,
    .widx = 0,
    .dline = left_comb1_dline
};

comb_data right_comb1_data = {
    .fb_gain = COMB1_DEFAULT_GAIN,
    .N = 1687,
    .dline_length = 2300,
    .widx = 0,
    .dline = right_comb1_dline
};

comb_data left_comb2_data = {
    .fb_gain = COMB2_DEFAULT_GAIN,
    .N = 1601,
    .dline_length = 2300,
    .widx = 0,
    .dline = left_comb2_dline
};

comb_data right_comb2_data = {
    .fb_gain = COMB2_DEFAULT_GAIN,
    .N = 1601,
    .dline_length = 2300,
    .widx = 0,
    .dline = right_comb2_dline
};

comb_data left_comb3_data = {
    .fb_gain = COMB3_DEFAULT_GAIN,
    .N = 2035,
    .dline_length = 2300,
    .widx = 0,
    .dline = left_comb3_dline
};

comb_data right_comb3_data = {
    .fb_gain = COMB3_DEFAULT_GAIN,
    .N = 2035,
    .dline_length = 2300,
    .widx = 0,
    .dline = right_comb3_dline
};

comb_data left_comb4_data = {
    .fb_gain = COMB4_DEFAULT_GAIN,
    .N = 2251,
    .dline_length = 2300,
    .widx = 0,
    .dline = left_comb4_dline
};

comb_data right_comb4_data = {
    .fb_gain = COMB4_DEFAULT_GAIN,
    .N = 2251,
    .dline_length = 2300,
    .widx = 0,
    .dline = right_comb4_dline
};

sample_t temp_buffer_a_l[1024];
sample_t temp_buffer_a_r[1024];
sample_t temp_buffer_b_l[1024];
sample_t temp_buffer_b_r[1024];

double dry_proportion = 0.5;
int use_ap3 = 1;

void reverb_process(jack_nframes_t nframes, sample_t *in_l, sample_t *in_r, sample_t *out_l, sample_t *out_r)
{
    //allpass stages
    allpass_process(&left_ap1_data, nframes, in_l, temp_buffer_a_l);
    allpass_process(&right_ap1_data, nframes, in_r, temp_buffer_a_r);

    allpass_process(&left_ap2_data, nframes, temp_buffer_a_l, temp_buffer_b_l);
    allpass_process(&right_ap2_data, nframes, temp_buffer_a_r, temp_buffer_b_r);

    if (use_ap3){
        //allpass stage 3
        allpass_process(&left_ap3_data, nframes, temp_buffer_b_l, temp_buffer_a_l);
        allpass_process(&right_ap3_data, nframes, temp_buffer_b_r, temp_buffer_a_r);
    } else {
        memcpy(temp_buffer_a_l, temp_buffer_b_l, nframes * sizeof(sample_t));
        memcpy(temp_buffer_a_r, temp_buffer_b_r, nframes * sizeof(sample_t));
    }

    //parallel comb 1
    comb_filter(&left_comb1_data, nframes, temp_buffer_a_l , temp_buffer_b_l);
    comb_filter(&right_comb1_data, nframes, temp_buffer_a_r , temp_buffer_b_r);
    for(int i = 0; i < nframes; i++){
        out_l[i] = (1-dry_proportion)* temp_buffer_b_l[i] + dry_proportion * in_l[i];
        out_r[i] = (1-dry_proportion)* temp_buffer_b_r[i] + dry_proportion * in_r[i];
    }

    //parallel comb 2
    comb_filter(&left_comb2_data, nframes, temp_buffer_a_l, temp_buffer_b_l);
    comb_filter(&right_comb2_data, nframes, temp_buffer_a_r, temp_buffer_b_r);

    for(int i = 0; i < nframes; i++){
        out_l[i] += (1-dry_proportion)* temp_buffer_b_l[i] + dry_proportion * in_l[i];
        out_r[i] += (1-dry_proportion)* temp_buffer_b_r[i] + dry_proportion * in_r[i];
    }

    //parallel comb 3
    comb_filter(&left_comb3_data, nframes, temp_buffer_a_l, temp_buffer_b_l);
    comb_filter(&right_comb3_data, nframes, temp_buffer_a_r, temp_buffer_b_r);

    for(int i = 0; i < nframes; i++){
        out_l[i] += (1-dry_proportion)* temp_buffer_b_l[i] + dry_proportion * in_l[i];
        out_r[i] += (1-dry_proportion)* temp_buffer_b_r[i] + dry_proportion * in_r[i];
    }

    //parallel comb 4
    comb_filter(&left_comb4_data, nframes, temp_buffer_a_l, temp_buffer_b_l);
    comb_filter(&right_comb4_data, nframes, temp_buffer_a_r, temp_buffer_b_r);

    for(int i = 0; i < nframes; i++){
        out_l[i] += (1-dry_proportion)* temp_buffer_b_l[i] + dry_proportion * in_l[i];
        out_r[i] += (1-dry_proportion)* temp_buffer_b_r[i] + dry_proportion * in_r[i];
    }
}
