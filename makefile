CC=clang
CFLAGS = `pkg-config gtk+-2.0 --cflags`
LDFLAGS=-ljack -lm `pkg-config gtk+-2.0 --libs`
DEPS = comb.h convenience.h allpass.h modulated_delay_line.h jack_config.h gui.h reverb.h
OBJ = main.o comb.o allpass.o modulated_delay_line.o jack_config.o gui.o reverb.o

all: effect

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

effect: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

clean:
	rm -f effect
	rm -f *.o
